package edu.umd.cs.submit;

import org.apache.hc.client5.http.fluent.Request;
import org.apache.hc.core5.net.URIBuilder;
import org.apache.hc.core5.util.Timeout;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import static edu.umd.cs.submit.CommandLineSubmit.HTTP_TIMEOUT;

record SubmitUser(String cvsAccount, String oneTimePassword) {
    public static SubmitUser from(File submitUser, Submit submit) throws IOException, URISyntaxException {
        final var submitUserProps = new Properties();
        submitUser.createNewFile();
        if (submitUser.canRead()) {
            submitUserProps.load(new FileInputStream(submitUser));
            if (submitUserProps.getProperty("cvsAccount") == null || submitUserProps.getProperty("oneTimePassword") == null) {
                if (!submitUser.canWrite()) {
                    throw new IOException(".submitUser not writable");
                }
                final var creds = Credentials.getFromUser();
                createSubmitUserFile(submitUser, submit, creds);
                submitUserProps.load(new FileInputStream(submitUser));
            }

        } else {
            throw new IllegalArgumentException("Can't generate or access " + submitUser);
        }

        final var cvsAccount = submitUserProps.getProperty("cvsAccount");
        final var oneTimePassword = submitUserProps.getProperty("oneTimePassword");

        return new SubmitUser(cvsAccount, oneTimePassword);
    }

    private static void createSubmitUserFile(File submitUser, Submit submit, Credentials creds) throws IOException, URISyntaxException {
        final var uri = new URIBuilder(
                new URI(submit.submitURL()
                        .replaceFirst("/eclipse/.*", "/eclipse/NegotiateOneTimePassword")
                )
        )
                .addParameter("campusUID", creds.user())
                .addParameter("uidPassword", creds.pass())
                .addParameter("courseName", submit.courseName())
                .addParameter("semester", submit.semester())
                .addParameter("projectNumber", submit.projectNumber())
                .build();

        final var post = Request
                .post(uri)
                .connectTimeout(Timeout.ofMilliseconds(HTTP_TIMEOUT))
                .execute();

        final var content = post.returnContent();

        try (final var bufferedReader = new BufferedReader(new InputStreamReader(content.asStream()));
             final var printWriter = new PrintWriter(new FileWriter(submitUser))
        ) {
            bufferedReader.lines().forEach(printWriter::println);
        }
    }

    public static void main(String[] args) throws Exception {
        var submit = Submit.from(new File(".submit"));
        var submitUser = SubmitUser.from(new File(".submitUser"), submit);
        System.out.println(submitUser);
    }
}

record Credentials(String user, String pass) {
    public static Credentials getFromUser() throws IOException {
        System.out.println("We need to authenticate you and create a .submitUser file so you can submit from this directory");
        System.out.println("Please enter your UMD Directory ID and password");
        System.out.print("Directory ID: ");
        final var directoryId = new BufferedReader(new InputStreamReader(System.in)).readLine();
        System.out.println("Password: ");
        final var password = String.valueOf(System.console().readPassword());
        System.out.println("Thanks!");
        return new Credentials(directoryId, password);
    }
}