// 
// Decompiled by Procyon v0.5.36
// 

package edu.umd.cs.submit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

public class FilesToIgnore {
    private static final String[] baseIgnoredPatterns = new String[]{
            "RCS",
            "SCCS",
            "CVS",
            "CVS.adm",
            "RCSLOG",
            "cvslog.*",
            "tags",
            "TAGS",
            ".make.state",
            ".nse_depinfo",
            "*~",
            "#*",
            ".#*",
            ",*",
            "_$*",
            "*$",
            "*.old",
            "*.bak",
            "*.BAK",
            "*.orig",
            "*.rej",
            ".del-*",
            "*.a",
            "*.olb",
            "*.o",
            "*.obj",
            "*.so",
            "*.exe",
            "*.Z",
            "*.elc",
            "*.ln",
            "*.class",
            "core"
    };
    final StringBuilder patternBuffer;

    public FilesToIgnore(StringBuilder patternBuffer) {
        this.patternBuffer = patternBuffer;
    }

    public FilesToIgnore() {
        this(new StringBuilder(
                String.join("|", baseIgnoredPatterns)
        ));
    }


    public void addPattern(final String str) {
        this.patternBuffer.append('|');
        this.patternBuffer.append(str);
    }

    public void addIgnoredPatternsFromFile(final File file) throws IOException {
        new BufferedReader(new FileReader(file)).lines().forEach(this::addPattern);
    }

    public Pattern getPattern() {
        return Pattern.compile(this.patternBuffer.toString().replaceAll("\\$",
                "\\\\\\$").replaceAll("\\.",
                "\\\\.").replaceAll("\\*",
                "\\.\\*").replaceAll("\\?",
                "\\."));
    }

    public static void main(final String[] array) {
        final FilesToIgnore filesToIgnore = new FilesToIgnore();
        filesToIgnore.addPattern("foo");
        filesToIgnore.addPattern("foo?");
        filesToIgnore.addPattern("foo$");
        filesToIgnore.addPattern("foo.bar");
        filesToIgnore.addPattern("foo.*");
        final String[] split = filesToIgnore.getPattern().pattern().split("\\|");
        for (String s : split) {
            System.out.println(s);
        }
    }
}
