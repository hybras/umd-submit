package edu.umd.cs.submit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

record NecessaryFiles(File submit, File submitUser, File submitIgnore, File cvsignore) {
    public NecessaryFiles {
        if (!submit.canRead()) {
            throw new IllegalArgumentException("Must perform submit from a directory containing a readable \".submit\" file");
        }
        try {
            if (Files.isSymbolicLink(submit.toPath())) {

                final var old = Path.of(".submit.old");
                Files.move(submit.toPath(), old);
                Files.copy(old, submit.toPath());

            }
            Files.deleteIfExists(submitUser.toPath());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
