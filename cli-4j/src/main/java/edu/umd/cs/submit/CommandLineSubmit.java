// 
// Decompiled by Procyon v0.5.36
// 

package edu.umd.cs.submit;

import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.fluent.Request;
import org.apache.hc.core5.net.URIBuilder;
import org.apache.hc.core5.util.Timeout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CommandLineSubmit {
    public static final String VERSION = "0.1.1";
    public static final int HTTP_TIMEOUT = Integer.getInteger("HTTP_TIMEOUT", 30) * 1000;

    public static final String finalMessage = """
            Note: you're responsible for logging into the submit server to verify that
            your submission compiled and worked properly (once the public tests are
            available).  Correct and resubmit it if not.
                        
            Also be certain to carefully read the project requirements and submission
            requirements, and be sure you understand the project grading policies in the
            course syllabus.
            """;

    public static void main(final String[] array) throws Exception {

        final var files = new NecessaryFiles(new File(".submit"), new File(".submitUser"), new File(".submitIgnore"), new File(".cvsignore"));
        final var submit = Submit.from(files.submit());

        System.out.println("Submitting project " + submit.projectNumber() + " for " + submit.courseName());
        final var allFiles = getIncludedFiles(files);

        final var submitUser = SubmitUser.from(files.submitUser(), submit);

        System.out.println("Submitting the following files");

        final var out = zipFilesToStream(allFiles);
        submitZipFile(submit, submitUser, out);
        System.out.print(finalMessage);

    }

    private static void submitZipFile(Submit submit, SubmitUser submitUser, ByteArrayOutputStream out) throws URISyntaxException, IOException {
        final var uri = new URIBuilder(submit.submitURL())
                .addParameter("cvsAccount", submitUser.cvsAccount())
                .addParameter("oneTimePassword", submitUser.oneTimePassword())
                .addParameter("courseName", submit.courseName())
                .addParameter("projectNumber", submit.projectNumber())
                .addParameter("semester", submit.semester())
                .addParameter("submitClientTool", "CommandLineTool")
                .addParameter("submitClientVersion", VERSION)
                .build();

        final var body = MultipartEntityBuilder.create()
                .addBinaryBody("submittedFiles", out.toByteArray())
                .build();

        final var res = Request.post(uri)
                .connectTimeout(Timeout.ofMilliseconds(HTTP_TIMEOUT))
                .body(body)
                .execute();

        System.out.println(res.returnContent().asString());
    }

    private static Collection<File> getIncludedFiles(NecessaryFiles files) throws IOException {
        final var filesToIgnore = new FilesToIgnore();
        filesToIgnore.addIgnoredPatternsFromFile(files.cvsignore());
        filesToIgnore.addIgnoredPatternsFromFile(files.submitIgnore());
        final var findAllFiles = new FindAllFiles(files.submit(), filesToIgnore.getPattern());
        return findAllFiles.getAllFiles();
    }

    private static ByteArrayOutputStream zipFilesToStream(Collection<File> allFiles) throws IOException {
        final var out = new ByteArrayOutputStream(4096);
        final var array2 = new byte[4096];
        final var zipOutputStream = new ZipOutputStream(out);
        zipOutputStream.setComment("zipfile for CommandLineTurnin, version " + VERSION);
        for (final File file : allFiles) {
            if (file.isDirectory()) {
                continue;
            }
            final var name = file.getName();
            System.out.println(name);
            final var e = new ZipEntry(name);
            e.setTime(file.lastModified());
            zipOutputStream.putNextEntry(e);
            try (var fileInputStream = new FileInputStream(file)) {
                while (true) {
                    final var read = fileInputStream.read(array2);
                    if (read < 0) {
                        break;
                    }
                    zipOutputStream.write(array2, 0, read);
                }
            }
            zipOutputStream.closeEntry();
        }
        zipOutputStream.close();
        return out;
    }

}
