// 
// Decompiled by Procyon v0.5.36
// 

package edu.umd.cs.submit;


import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.file.NotDirectoryException;
import java.util.*;
import java.util.regex.Pattern;

public class FindAllFiles
{
    final Set<File> files;
    final File root;
    final Pattern pattern;
    
    FindAllFiles(final File file, final Pattern pattern) throws IOException {
        this.pattern = pattern;
        this.root = file.getCanonicalFile().getParentFile();
        this.files = this.searchFrom(this.root);
    }

    private Set<File> searchFrom(@Nonnull final File root) throws IOException {
        if (root.isDirectory()) {
            final var listFiles = Objects.requireNonNull(root.listFiles());
            final var files = new TreeSet<File>();
            files.add(root);
            for (final var listFile : listFiles) {
                final var canonicalFile = listFile.getCanonicalFile();
                if (canonicalFile.toPath().startsWith(this.root.toPath())
                        && !this.pattern.matcher(canonicalFile.getName()).matches()
                        // Mistakenly adds directories?
                        && files.add(canonicalFile) && canonicalFile.isDirectory()
                ) {
                    files.addAll(this.searchFrom(canonicalFile));
                }
            }
            return files;
        } else throw new NotDirectoryException(root.getPath());
    }
    
    public Collection<File> getAllFiles() {
        return this.files;
    }
    
    public static void main(final String[] array) throws IOException {
        final var ignore = new FilesToIgnore();
        ignore.addIgnoredPatternsFromFile(new File(".submitIgnore"));
        final var findAllFiles = new FindAllFiles(
                new File(".submit"),
                ignore.getPattern());
        System.out.println("---");
        findAllFiles
                .getAllFiles()
                .stream().filter(it -> !it.isDirectory())
                .forEach(System.out::println);
    }
}
