use std::io::{BufReader, BufWriter, Read};
use anyhow::{Context, Result};
use ignore::{DirEntry, WalkBuilder};
use std::convert::TryFrom;
use std::fs::File;
use std::path::{Path, PathBuf};
use reqwest::blocking::{Client, Response};

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

pub const DOT_SUBMIT: &str = ".submit";

pub const SUBMIT_URL: &str = "submitURL";
pub const COURSE_NAME: &str = "courseName";
pub const SEMESTER: &str = "semester";
pub const PROJECT_NUMBER: &str = "projectNumber";

#[derive(Debug)]
pub struct ProjectInfo {
    pub submit_url: String,
    pub course: String,
    pub semester: String,
    pub project: String,
}

impl <R: Read> TryFrom<BufReader<R>> for ProjectInfo {
    type Error = anyhow::Error;

    fn try_from(value: BufReader<R>) -> Result<Self, Self::Error> {
        use java_properties::read;

        let mut map = read(value)?;

        // TODO error handling for missing properties
        let submit_url = map.remove(SUBMIT_URL).unwrap();
        let course_name = map.remove(COURSE_NAME).unwrap();
        let semester = map.remove(SEMESTER).unwrap();
        let project_number = map.remove(PROJECT_NUMBER).unwrap();

        Ok(Self {
            submit_url,
            course: course_name,
            semester,
            project: project_number,
        })
    }
}

#[derive(Debug)]
pub struct Submit {
    pub props: ProjectInfo,
    pub root: PathBuf,
}

impl Submit {
    pub fn new() -> Result<Self> {
        let path = Path::new(DOT_SUBMIT);
        Self::with_path(path)
    }

    pub fn with_path(path: &Path) -> Result<Self> {
        let file = File::open(path).with_context(|| format!("Couldn't open {}", DOT_SUBMIT))?;
        let reader = BufReader::new(file);
        let root = path
            .parent()
            .context("Couldn't find project root path")
            .unwrap()
            .to_path_buf();
        Ok(Self {
            props: ProjectInfo::try_from(reader)?,
            root,
        })
    }

    pub fn files(&self) -> Result<impl Iterator<Item = DirEntry>> {
        let paths = WalkBuilder::new(&self.root)
            .add_custom_ignore_filename(".submitIgnore")
            .require_git(false)
            .build()
            .filter_map(|it| it.ok());

        Ok(paths)
    }

    pub fn submit<R: Read + Send + 'static>(
        self,
        zip: R,
        user: SubmitUser,
        client: &Client,
    ) -> Result<Response> {
        use reqwest::blocking::multipart::{Form, Part};

        let tool_key = "submitClientTool";
        let tool_val = "submit-rs".to_owned();
        let version_key = "submitClientVersion";

        let res = client
            .post(&self.props.submit_url)
            .query(&[
                (CVS_ACCOUNT, &user.cvs_account),
                (ONE_TIME_PASSWORD, &user.one_time_password),
                (COURSE_NAME, &self.props.course),
                (PROJECT_NUMBER, &self.props.project),
                (SEMESTER, &self.props.semester),
                (tool_key, &tool_val),
                (version_key, &VERSION.to_owned()),
            ])
            .multipart(
                Form::new().part("submittedFiles", Part::reader(zip).file_name("submit.zip")),
            )
            .send()?;

        Ok(res)
    }
}

impl<'a> TryFrom<&'a Path> for Submit {
    type Error = anyhow::Error;

    fn try_from(path: &'a Path) -> Result<Self> {
        Submit::with_path(path)
    }
}

#[derive(Debug)]
pub struct SubmitUser {
    cvs_account: String,
    /// one time password
    one_time_password: String,
}

pub const CVS_ACCOUNT: &str = "cvsAccount";
pub const ONE_TIME_PASSWORD: &str = "oneTimePassword";

impl SubmitUser {
    pub fn new(props: &ProjectInfo, user: String, pass: String, client: &Client) -> Result<SubmitUser> {
        let campus_uid = "campusUID";
        let uid_password = "uidPassword";

        let res = client.post(
            &props
                .submit_url
                .replace("SubmitProjectViaEclipse", "NegotiateOneTimePassword"),
        )
            .query(
                &[
                    (campus_uid, &user),
                    (uid_password, &pass),
                    (COURSE_NAME, &props.course),
                    (SEMESTER, &props.semester),
                    (PROJECT_NUMBER, &props.project),
                ]
            ).send()?;

        let mut user_props = java_properties::read(res)?;

        Ok(Self {
            cvs_account: user_props.remove(CVS_ACCOUNT).unwrap(),
            one_time_password: user_props.remove(ONE_TIME_PASSWORD).unwrap(),
        })
    }
}

pub fn zipfile(mut files: impl Iterator<Item = DirEntry>, root: &Path) -> Result<File> {
    use tempfile::tempfile;
    use zip::ZipWriter;

    let out = tempfile()?;
    let out = BufWriter::new(out);
    let mut out = ZipWriter::new(out);
    let comment = format!("zipfile for CommandLineTurnin, version {}", VERSION);
    out.set_comment(comment);

    files.try_for_each(|entry| -> Result<()> {
        let path = entry.path();
        let path_str = path.strip_prefix(root)?.to_str().unwrap();
        if path.is_dir() {
            out.add_directory(path_str, Default::default())?;
        } else {
            let file = File::open(path)?;
            let mut reader = BufReader::new(file);
            out.start_file(path_str, Default::default())?;
            std::io::copy(&mut reader, &mut out)?;
        }
        Ok(())
    })?;

    let archive = out.finish().unwrap().into_inner()?;
    Ok(archive)
}

pub fn submit(path: Option<&Path>, user: String, pass: String) -> Result<()>{
    let submit = match path {
        Some(path) => Submit::with_path(path),
        None => Submit::new(),
    }?;
    let http = Client::new();
    let user = SubmitUser::new(&submit.props, user, pass, &http)?;
    let files = submit.files()?;
    let zip = zipfile(files, &submit.root)?;

    submit.submit(zip, user, &http)?;
    Ok(())

}
