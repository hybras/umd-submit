package edu.umd.cs.submit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

record Submit(String submitURL, String courseName, String semester, String projectNumber) {
    public static Submit from(File submit) throws IOException {
        final var submitProps = new Properties();
        submitProps.load(new FileInputStream(submit));

        final String submitURL = Objects.requireNonNull(submitProps.getProperty("submitURL"), ".submit file does not contain a submitURL");

        final String courseName = submitProps.getProperty("courseName");
        final String semester = submitProps.getProperty("semester");
        final String projectNumber = submitProps.getProperty("projectNumber");

        return new Submit(submitURL, courseName, semester, projectNumber);
    }
}
