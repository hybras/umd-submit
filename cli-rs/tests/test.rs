use std::path::Path;

use anyhow::Result;
use submit::{zipfile, Submit, SubmitUser};
use reqwest::blocking::Client;

load_dotenv::load_dotenv!();

#[test]
fn display_files() -> Result<()> {
    let submit = dbg!(Submit::with_path(Path::new(
        "./tests/draw_figures/.submit"
    ))?);
    let files = submit.files()?;
    assert_eq!(files.count(), 2);
    Ok(())
}

#[test]
fn test_zip() -> Result<()> {
    let submit = dbg!(Submit::with_path(Path::new(
        "./tests/draw_figures/.submit"
    ))?);
    let files = submit.files()?;
    let zip = zipfile(files, &submit.root)?;

    let zip = zip::ZipArchive::new(zip)?;

    assert!(zip.file_names().find(|it| *it == "colors_in_c.c").is_some());

    Ok(())
}

#[test]
fn test_submit() -> Result<()> {
    let client = Client::new();
    let submit = Submit::with_path(Path::new("./tests/draw_figures/.submit"))?;
    let files = submit.files()?;
    let zip = zipfile(files, &submit.root)?;

    let submit_user = SubmitUser::new(
        &submit.props,
        env!("UID").to_string(),
        env!("PASS").to_string(),
        &client
    )?;

    submit.submit(zip, submit_user, &client)?;

    Ok(())
}
