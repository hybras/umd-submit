use std::io::Write;

use anyhow::Result;
use console::Term;
use dialoguer::{Input, Password};
use marmoset_submit::{zipfile, ProjectInfo, Submit, SubmitUser};
use reqwest::blocking::Client;

const FINAL_MESSAGE: &str = r#"
Note: you're responsible for logging into the submit server to verify that
your submission compiled and worked properly (once the public tests are
available).  Correct and resubmit it if not.
            
Also be certain to carefully read the project requirements and submission
requirements, and be sure you understand the project grading policies in the
course syllabus.
"#;

fn main() -> Result<()> {
    let mut term = Term::stdout();
    let http_client = Client::new();

    let submit = Submit::new()?;
    let user = creds(&submit.props, &http_client)?;

    writeln!(
        term,
        "Submitting project {} for {}",
        submit.props.project, submit.props.course
    )?;

    let files = submit.files()?;
    let zip = zipfile(files, &submit.root)?;

    submit.submit(zip, user, &http_client)?;

    writeln!(term, "{}", FINAL_MESSAGE)?;
    Ok(())
}

fn creds(props: &ProjectInfo, http_client: &Client) -> Result<SubmitUser> {
    let user = Input::<String>::new()
        .with_prompt("UMD Directory ID")
        .interact()?;
    let pass = Password::new().with_prompt("Password").interact()?;
    SubmitUser::new(props, user, pass, &http_client)
}
